package com.bullhead.kokafx.convertor;

import javafx.util.StringConverter;

public class StringLongConverter extends StringConverter<Long> {
    @Override
    public String toString(Long object) {
        if (object != null && object != -1 && object != 0) {
            return String.valueOf(object);
        }
        return "";
    }

    @Override
    public Long fromString(String string) {
        try {
            return Long.parseLong(string);
        } catch (Exception e) {
            return 0L;
        }
    }
}
