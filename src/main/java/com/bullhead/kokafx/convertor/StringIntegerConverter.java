package com.bullhead.kokafx.convertor;

import javafx.util.StringConverter;

public class StringIntegerConverter extends StringConverter<Integer> {
    @Override
    public String toString(Integer object) {
        if (object != null && object != -1 && object != 0) {
            return String.valueOf(object);
        }
        return "";
    }

    @Override
    public Integer fromString(String string) {
        try {
            return Integer.parseInt(string);
        } catch (Exception e) {
            return 0;
        }
    }
}
