package com.bullhead.kokafx.view;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.control.TableColumn;

public class WeightTableColumn<S, T> extends TableColumn<S, T> {
    private final DoubleProperty weight = new SimpleDoubleProperty(this, "weight", 1);

    public final DoubleProperty weightProperty() {
        return weight;
    }

    public final Double getWeight() {
        return weight.get();
    }

    public final void setWeight(Double value) {
        if (value < 0) {
            throw new IllegalArgumentException("Weight cannot be negative");
        }
        weight.set(value);
    }


}
