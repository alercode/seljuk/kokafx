package com.bullhead.kokafx.view;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.scene.Node;
import javafx.scene.control.Label;

public class KokaLabel extends Label {
    private final BooleanProperty        textAllCaps  = new SimpleBooleanProperty(this, "textAllCaps", false);
    private       String                 originalText;
    private final ChangeListener<String> textListener = ((observable, oldValue, newValue) -> {
        if (!newValue.equalsIgnoreCase(originalText)) {
            originalText = newValue;
            toggleTextAllCaps();
        }
    });

    public KokaLabel() {
        initialize("");
    }

    public KokaLabel(String text) {
        super(text);
        initialize(text);
    }

    public KokaLabel(String text, Node graphic) {
        super(text, graphic);
        initialize(text);
    }

    public final boolean isTextAllCaps() {
        return textAllCaps.get();
    }

    public final void setTextAllCaps(boolean textAllCaps) {
        this.textAllCaps.set(textAllCaps);
        toggleTextAllCaps();
    }

    public final BooleanProperty textAllCapsProperty() {
        return textAllCaps;
    }

    private void toggleTextAllCaps() {
        String text = getText();
        if (text != null && text.length() > 0) {
            textProperty().removeListener(textListener);
            setText(textAllCaps.get() ? text.toUpperCase() : originalText);
            textProperty().addListener(textListener);
        }
    }

    private void initialize(String text) {
        originalText = text;
        textProperty().addListener(textListener);
    }
}
