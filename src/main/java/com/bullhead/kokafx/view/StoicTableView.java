package com.bullhead.kokafx.view;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class StoicTableView<S> extends TableView<S> {
    private final DoubleProperty weightSum = new SimpleDoubleProperty(this, "weightSum", 1);


    public StoicTableView() {
        addListenersForWeight();
    }

    public StoicTableView(ObservableList<S> items) {
        super(items);
        addListenersForWeight();
    }

    public final DoubleProperty weightSumProperty() {
        return weightSum;
    }

    public final Double getWeightSum() {
        return weightSum.get();
    }

    public final void setWeightSum(Double value) {
        if (value < 0) {
            throw new IllegalArgumentException("Weight sum cannot be negative");
        }
        weightSum.set(value);
    }

    private void addListenersForWeight() {
        widthProperty().addListener((observable, oldValue, newValue) -> {
            updateColumnsWidth(newValue.doubleValue());
        });
        getColumns().addListener((ListChangeListener<TableColumn<S, ?>>) c -> {
            updateColumnsWidth(getWidth());
        });
    }

    @SuppressWarnings("unchecked")
    private void updateColumnsWidth(double tableWidth) {
        double sum  = weightSum.get();
        int    size = getColumns().size();
        if (sum < size) {
            sum = size;
        }
        double finalSum = sum;
        getColumns().forEach(col -> {
            if (col instanceof WeightTableColumn) {
                final var columnWeight = ((WeightTableColumn<S, ?>) col).getWeight();
                final var width        = (tableWidth / finalSum) * columnWeight;
                col.setPrefWidth(width);
            }
        });
    }


}
