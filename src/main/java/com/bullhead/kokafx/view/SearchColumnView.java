package com.bullhead.kokafx.view;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

public class SearchColumnView extends GridPane {
    private final Label highlightLabel;
    private final Label textLabel;


    public SearchColumnView() {
        highlightLabel = new Label();
        highlightLabel.setStyle("-fx-text-fill: #4caf50;");
        textLabel = new Label();
        add(highlightLabel, 0, 0);
        add(textLabel, 1, 0);
    }


    public void set(@Nullable String search, @NonNull String text) {
        highlightLabel.setText(search);
        if (search != null) {
            text = text.replace(search, "");
        }
        textLabel.setText(text);
    }
}
