package com.bullhead.kokafx.compiler;

import com.bullhead.kokafx.annotation.TableColumn;
import com.bullhead.kokafx.annotation.TableModel;
import com.google.auto.service.AutoService;
import com.squareup.javapoet.*;
import javafx.scene.control.cell.PropertyValueFactory;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@AutoService(Processor.class)
public class TableColumnProcessor extends AbstractProcessor {
    private Filer filer;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        filer = processingEnvironment.getFiler();
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        for (Element element : roundEnvironment.getElementsAnnotatedWith(TableModel.class)) {
            if (element.getKind() == ElementKind.CLASS) {
                TypeElement typeElement = (TypeElement) element;
                writeClass(typeElement);
            }
        }

        return true;
    }

    private void writeClass(TypeElement typeElement) {
        boolean               haveMethod = false;
        ParameterizedTypeName columnType;
        columnType = ParameterizedTypeName.get(ClassName.get(javafx.scene.control.TableColumn.class),
                TypeName.get(typeElement.asType()), TypeName.get(Object.class));
        MethodSpec.Builder methodSpec = MethodSpec.methodBuilder("getColumns")
                .addModifiers(Modifier.STATIC, Modifier.PUBLIC)
                .addCode("List<TableColumn<" + typeElement.getSimpleName() + ",Object>> columns = new $T();", ArrayList.class)
                .returns(ParameterizedTypeName.get(ClassName.get(List.class), columnType));

        for (Element element : typeElement.getEnclosedElements()) {
            if (element.getKind() == ElementKind.CLASS) {
                if (element.getAnnotation(TableModel.class) != null) {
                    writeClass(((TypeElement) element));
                }
            } else if (element.getKind() == ElementKind.FIELD) {
                TableColumn column = element.getAnnotation(TableColumn.class);
                if (column != null) {
                    haveMethod = true;
                    methodSpec.addCode(CodeBlock.builder()
                            .add(createField(element, column).toString())
                            .build());
                }
            }
        }

        if (haveMethod) {
            methodSpec.addCode("return columns;");
            TypeSpec typeSpec = TypeSpec.classBuilder("TableData" + typeElement.getSimpleName())
                    .addModifiers(Modifier.PUBLIC)
                    .addMethod(methodSpec.build())
                    .build();
            JavaFile javaFile = JavaFile.builder(typeElement.getEnclosingElement().toString(), typeSpec)
                    .build();
            try {
                javaFile.writeTo(filer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private CodeBlock createField(Element element, TableColumn tableColumn) {
        String fieldName = "column" + element.getSimpleName();
        FieldSpec fieldSpec = FieldSpec.builder(TypeName.get(javafx.scene.control.TableColumn.class),
                fieldName)
                .initializer("new $T($S)", javafx.scene.control.TableColumn.class, tableColumn.value())
                .build();
        return CodeBlock.builder()
                .add(fieldSpec.toString())
                .addStatement(fieldName + ".setCellValueFactory(new $T($S))",
                        PropertyValueFactory.class, element.getSimpleName().toString())
                .add("columns.add(" + fieldName + ");")
                .build();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> annotations = new HashSet<>();
        annotations.add(TableModel.class.getCanonicalName());
        annotations.add(TableColumn.class.getCanonicalName());
        return annotations;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }
}
