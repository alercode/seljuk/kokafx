package com.bullhead.kokafx.util;

import javafx.beans.property.SimpleStringProperty;

public class UppercaseStringProperty extends SimpleStringProperty {

    public UppercaseStringProperty() {

    }

    public UppercaseStringProperty(String initialValue) {
        super(initialValue);
    }

    @Override
    public String getValue() {
        String value = super.getValue();
        if (value != null) {
            return value.toUpperCase();
        }
        return "";
    }
}
