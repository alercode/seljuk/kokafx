package com.bullhead.kokafx.util;

import com.bullhead.kokafx.view.SearchColumnView;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

public class SearchableColumnFactory<T, X> implements Callback<TableColumn<T, X>, TableCell<T, X>> {
    private final StringProperty searched;

    public SearchableColumnFactory(StringProperty searched) {
        this.searched = searched;
    }

    @Override
    public TableCell<T, X> call(TableColumn<T, X> param) {
        return new TableCell<>() {
            @Override
            protected void updateItem(X item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty) {
                    SearchColumnView view = new SearchColumnView();
                    view.set(searched.get(), String.valueOf(item));
                    setGraphic(view);
                } else {
                    setGraphic(null);
                }
            }
        };
    }
}
